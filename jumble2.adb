--------------------------------------------------------------
-- Program: JumbleSolve              Date: March 4, 2018    --
-- Author: Dhruv Lad                 Student ID: 0928018    --
-- Class: CIS*3190                  Assignment: A2          --
-- Description: Find the word from user input.              --
--------------------------------------------------------------

with Ada.Characters.Handling;
with Ada.Command_Line;
with Ada.Strings.Bounded;
with Ada.Text_IO;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Unbounded.Text_IO; use Ada.Strings.Unbounded.Text_IO;

procedure jumble2 is

File_Name : constant String := "/usr/share/dict/canadian-english-small"; -- file path

package Words is new Ada.Strings.Bounded.Generic_Bounded_Length(6);
 use type Words.Bounded_String;
type Words_Array is array (Natural range <>) of Words.Bounded_String;
type Words_Ptr is access Words_Array; -- array of string 
user_array : Words.Bounded_String; --array of string bounded 


List       : Words_Ptr := new Words_Array (1 .. 50_000); -- create a array of string 
Last_Word  : Natural; -- Last word read into List.
   --finds the dictionary word from the user input 
  procedure findAnagram is 
  one : Natural := List'First; -- first of the list dictionary 
  mid : Natural; -- middle of the dictionary 
  up : Natural := Last_word; -- last of the dictoinary 
  Found : Boolean; -- booleean value 
  
  begin   
    -- finds the words by comparing two list
   while 1 = 1 loop
    mid := (one + up)/2; -- gets the mid of the list 
      Found := user_array = List(mid); 
      if user_array > List(mid) then
        one := mid + 1;
      else 
       up := mid -1;
      end if;
      if Found then -- if found is true 
        exit; -- found the word 
      else if one > up then -- if lower is greater than upper
        exit;   -- exit went through all of the list 
      end if; 
     end if;  
   end loop; 
   
   -- if found is true then output the result 
   if Found then  
     Ada.Text_IO.put_Line(Words.To_String(user_array));
   end if;     
   
  end findAnagram;
  
 --creates all the possible words from the the user input 
  procedure generateAnagram (user_word : in String; K : Positive:= 1) is 
  
Str : String(user_word'Range) := user_word; -- get the range of the user string
N : Positive := Str'Length; -- the length of user string
temp : Character; -- temporary character
  
  begin
     -- base case for recursive 
     if K = N then
       -- Ada.Text_IO.put_Line(Str); 
        user_array := Words.To_Bounded_String(Str);
        findAnagram; 
     else
        for i in K .. N loop -- for loop of string length 
           temp := Str(i); -- gets the character at i
           Str(i) := Str(K); 
           Str(K) := temp;
           generateAnagram(Str,K+1); -- call the function agian 
        end loop; -- ends the loop 
     end if;         
      
  end generateAnagram; 
  
  -- get the word from the user
  procedure inputjumble is 
  
  begin 
    Ada.Text_IO.put_Line("Enter the string: ");
    
	declare
	user_words : String := Ada.Text_IO.Get_Line;
	
	begin
	 
	   if user_words'Length > 6 then
	      Ada.Text_IO.put_Line("too long");
	  
	   else 
		  Ada.Text_IO.put_Line("------");
		  Ada.Text_IO.put_Line("Words that are Found");
	      generateAnagram(user_words);
	   end if;
	end;
	
  end inputjumble; 
   
  -- Read the dictionary for words up to 10 in length
  procedure BuildLexicon (List : Words_Ptr; Count : out Natural) is
   
    Dic_File : Ada.Text_IO.File_Type;  
    Line : String (1 .. 30); -- number of character to get from the line 
    Last : Natural; -- last line 
  begin
    Count := 0;
    Ada.Text_IO.Open(Dic_File, Ada.Text_IO.In_File, File_Name); -- opens the file
    
    while not Ada.Text_IO.End_Of_File(Dic_File) loop -- read file until its end of file
     
      Ada.Text_IO.Get_Line (Dic_File, Line, Last); -- get the line 
     
      if Last <= 6 then -- check if it less than 10
      
        Count := Count + 1; 
        Line(1 .. Last) := Ada.Characters.Handling.To_Lower(Line(1 .. Last)); -- convert the character to lowercase 
        List(Count) := Words.To_Bounded_String(Line(1 .. Last)); -- add it to the array 
     
      end if;
   
    end loop; -- end the while loop
   
    Ada.Text_IO.Close(Dic_File); -- close the file 
  
  end BuildLexicon; 
  
  begin 
  
  BuildLexicon (List, Last_Word); -- reads in the dictionary 
  loop  -- beginning of the loop
  inputJumble; -- call teh inputjumble procedure
  
  Ada.Text_IO.Put_Line("to quit(q) or to continue(c) ?"); -- asks the question to user
  
  declare -- declares the string
  char : String := Ada.Text_IO.Get_Line;
  
  begin
  
  if char = "q" then --checks if user asked to quit
    exit;
  end if;
  
  end;
     
  end loop; -- end the  loop
end jumble2;

  
